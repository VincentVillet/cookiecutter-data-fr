# Cookiecutter Projet data en local

Une structure standard pour initier un projet d'analyse de données en Python en local sur son poste.
 

### Dépendances nécessaire pour utiliser ce template cookiecutter

 - Python >=2.7 ou >=3.5
 - [Cookiecutter Python package](http://cookiecutter.readthedocs.org/en/latest/installation.html) >= 1.4.0: 
 Cela peut être installé via `pip` ou par `conda`, selon la façon dont vous gérez vos packages Python:

``` bash
$ pip install cookiecutter
```

ou

``` bash
$ conda config --add channels conda-forge
$ conda install cookiecutter
```


### Pour démarrer un nouveau projet, lancez la commande suivante:

    cookiecutter https://gitlab.com/VincentVillet/cookiecutter-data-fr

Créez ensuite votre repo git et faites le lien entre le dossier local sur votre ordinateur et le repo distant avec les commandes suivantes (pour github):
    
    cd <répertoire-du-projet>
    git init
    git add .
    git commit -m "first commit, cookiecutter template init."
    git remote add origin git@github.com:<votre-nom>/<nom-du-repo>.git
    git push -u origin master
                

Ce projet s'inspire fortement d’un [fork de cookiecutter-data-science développé par Pierre-Alain Jachiet](https://gitlab.com/pajachiet/cookiecutter-data-fr).


### La structure de dossier créée est la suivante
 

```

    ├── data
    │   ├── raw            <- La donnée originale, immutable.
    │   ├── interim        <- Données intermédiaires, qui ont été transformées.
    │   ├── output         <- Données de sortie du projet, et tous les graphiques associés.
    │   ├── models         <- Enregistrements des modèles entraînés. Les modèles sont systématiquement enregistrés après entraînement.
    │   └── logs           <- Sauvegarde des logs sous la forme d’un fichier par jour.
    │
    ├── notebooks          <- Notebooks Jupyter
    │
    ├── src                <- Code source du projet
    │   
    ├── tests              <- Tests du projet
    │
    ├── .gitignore
    ├── INSTALL.md
    ├── README.md    
    └── requirements.txt   <- Fichier de requirements pour reproduire l'environnement d'analyse (cf INSTALL.md)
```

## Exemple pédagogique
Les bonnes pratiques du projet sont illustrées sur une régression linéaire toute simple entre le PIB et la consommation de fuel au niveau mondial entre 1965 et 2008.
Sources:
pib mondial: https://pkgstore.datahub.io/core/gdp/gdp_csv/data/0048bc8f6228d0393d41cac4b663b90f/gdp_csv.csv
consommation d’énergie: https://openei.org/wiki/BP_Statistical_Review_of_World_Energy
inspiration: https://jancovici.com/transition-energetique/l-energie-et-nous/lenergie-de-quoi-sagit-il-exactement/

Les données ont été formatées à l’extérieur du projet et sauvegardées dans le github de ce template pour pouvoir être facilement téléchargeables depuis Internet.

## Contribuer

Les contributions sont bienvenues !